import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HeroService {

  baseURL: string = "http://localhost:8080/superheroes/";

  constructor(private http: HttpClient) { }

  getHeroes(page: number) : Observable<any> {
    const params = new HttpParams()
       .set('pageNumber', page.toString());

    const headers = new HttpHeaders()
      .set('Content-Type', 'text/plain')
      .set('Access-Control-Allow-Origin', '*');

    return this.http.get(this.baseURL, { 'params': params, 'headers': headers })
  }
}

import { Component, OnInit } from '@angular/core';
import { HeroService } from '../../service/hero.service';

@Component({
  selector: 'app-heroes-list',
  providers: [HeroService],
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.scss']
})
export class HeroesListComponent implements OnInit {

  currentPage = 0;
  heroes = [];

  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    this.refresh();
  }

  nextPage() {
  console.log(this.heroes.length);
    if (this.heroes.length == 20) {
      this.currentPage++;
      this.refresh();
    }
  }

  previousPage() {
      if (this.currentPage > 0) {
        this.currentPage--;
        this.refresh();
      }
    }

  refresh() {
    this.heroService.getHeroes(this.currentPage).subscribe(data => {
      this.heroes = data
      console.log(data);
    });
  }
}
